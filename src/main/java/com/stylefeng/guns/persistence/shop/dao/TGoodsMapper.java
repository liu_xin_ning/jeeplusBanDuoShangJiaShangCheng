package com.stylefeng.guns.persistence.shop.dao;

import com.stylefeng.guns.persistence.shop.model.TGoods;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface TGoodsMapper extends BaseMapper<TGoods> {

    List<TGoods> selectProductByFloor(Long id);

    List<TGoods> getProductByFloorid(Long tid);

    List<TGoods> selectgoodsListByType(TGoods g);
}