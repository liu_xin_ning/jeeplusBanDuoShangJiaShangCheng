package com.stylefeng.guns.modular.shop.controller;

import com.stylefeng.guns.common.annotion.Permission;
import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Const;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.shop.service.ITOrderLogService;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import com.stylefeng.guns.persistence.shop.model.TOrderLog;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单处理历史表 前端控制器
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Controller
@RequestMapping("/tOrderLog")
public class TOrderLogController extends BaseController {
    private String PREFIX = "/shop/tOrderLog/";

    @Resource
    ITOrderLogService itOrderLogService;


    /**
     * 跳转到部门管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tOrderLog.html";
    }

    /**
     * 跳转到添加部门
     */
    @RequestMapping("/tOrderLog_add")
    public String tOrderLogAdd() {
        return PREFIX + "tOrderLog_add.html";
    }

    /**
     * 跳转到修改部门
     */
    @RequestMapping("/tOrderLog_update/{tOrderLogId}")
    public String tOrderLogUpdate(@PathVariable Integer tOrderLogId, Model model) {
        TOrderLog tOrderLog = itOrderLogService.selectById(tOrderLogId);
        model.addAttribute(tOrderLog);
        LogObjectHolder.me().set(tOrderLog);
        return PREFIX + "tOrderLog_edit.html";
    }


    /**
     * 新增部门
     */
    @BussinessLog(value = "添加部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/add")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object add(TOrderLog tOrderLog) {
        return this.itOrderLogService.insert(tOrderLog);
    }

    /**
     * 获取所有部门列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String, Object>> list = this.itOrderLogService.selectMaps(null);
        return super.warpObject(new DeptWarpper(list));
    }

    /**
     * 部门详情
     */
    @RequestMapping(value = "/detail/{tOrderLogId}")
    @ResponseBody
    public Object detail(@PathVariable("tOrderLogId") Integer tOrderLogId) {
        return itOrderLogService.selectById(tOrderLogId);
    }

    /**
     * 修改部门
     */
    @BussinessLog(value = "修改部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/update")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object update(TOrderLog tOrderLog) {
        if (ToolUtil.isEmpty(tOrderLog) || tOrderLog.getId() == null) {
            throw new BussinessException(BizExceptionEnum.REQUEST_NULL);
        }
        itOrderLogService.updateById(tOrderLog);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除部门
     */
    @BussinessLog(value = "删除部门", key = "tOrderLogId", dict = Dict.DeleteDict)
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object delete(@RequestParam Integer tOrderLogId) {

        //缓存被删除的部门名称
        LogObjectHolder.me().set(ConstantFactory.me().getDeptName(tOrderLogId));

        itOrderLogService.deleteById(tOrderLogId);

        return SUCCESS_TIP;
    }
}
