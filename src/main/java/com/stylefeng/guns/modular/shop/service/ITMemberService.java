package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.persistence.shop.model.TMember;
import com.baomidou.mybatisplus.service.IService;
import me.chanjar.weixin.common.exception.WxErrorException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITMemberService extends IService<TMember> {
    /**
     * 合并微信用户信息
     *
     * @param wxOauthCode
     * @return
     */
    TMember mergeUserInfo(String wxOauthCode);

    /**
     * 获取微信用户
     *
     * @param wxOauthCode
     * @return
     */
    TMember getByWxOauthCode(String wxOauthCode) throws WxErrorException;

    /**
     * 根据 微信openid查询
     * @param openid
     * @return
     */
    TMember getByWxOpenId(String openid);
    TMember checkUser(String username, String password);
}
