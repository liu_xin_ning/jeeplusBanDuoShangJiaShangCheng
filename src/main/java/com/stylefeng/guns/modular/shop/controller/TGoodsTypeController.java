package com.stylefeng.guns.modular.shop.controller;

import com.stylefeng.guns.common.annotion.Permission;
import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Const;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.shop.service.ITGoodsTypeService;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import com.stylefeng.guns.persistence.shop.model.TGoodsType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Controller
@RequestMapping("/tGoodsType")
public class TGoodsTypeController extends BaseController {
    private String PREFIX = "/shop/tGoodsType/";

    @Resource
    ITGoodsTypeService itGoodsTypeService;


    /**
     * 跳转到部门管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tGoodsType.html";
    }

    /**
     * 跳转到添加部门
     */
    @RequestMapping("/tGoodsType_add")
    public String tGoodsTypeAdd() {
        return PREFIX + "tGoodsType_add.html";
    }

    /**
     * 跳转到修改部门
     */
    @RequestMapping("/tGoodsType_update/{tGoodsTypeId}")
    public String tGoodsTypeUpdate(@PathVariable Long tGoodsTypeId, Model model) {
        TGoodsType tGoodsType = itGoodsTypeService.selectById(tGoodsTypeId);
        model.addAttribute("tGoodsType",tGoodsType);
        LogObjectHolder.me().set(tGoodsType);
        return PREFIX + "tGoodsType_edit.html";
    }


    /**
     * 新增部门
     */
    @BussinessLog(value = "添加部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/add")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object add(TGoodsType tGoodsType) {
        return this.itGoodsTypeService.insert(tGoodsType);
    }

    /**
     * 获取所有部门列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String, Object>> list = this.itGoodsTypeService.selectMaps(null);
        return super.warpObject(new DeptWarpper(list));
    }

    /**
     * 部门详情
     */
    @RequestMapping(value = "/detail/{tGoodsTypeId}")
    @ResponseBody
    public Object detail(@PathVariable("tGoodsTypeId") Integer tGoodsTypeId) {
        return itGoodsTypeService.selectById(tGoodsTypeId);
    }

    /**
     * 修改部门
     */
    @BussinessLog(value = "修改部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/update")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object update(TGoodsType tGoodsType) {
        if (ToolUtil.isEmpty(tGoodsType) || tGoodsType.getId() == null) {
            throw new BussinessException(BizExceptionEnum.REQUEST_NULL);
        }
        itGoodsTypeService.updateById(tGoodsType);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除部门
     */
    @BussinessLog(value = "删除部门", key = "tGoodsTypeId", dict = Dict.DeleteDict)
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object delete(@RequestParam Long tGoodsTypeId) {

        itGoodsTypeService.deleteById(tGoodsTypeId);

        return SUCCESS_TIP;
    }
}
