package project.config;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/***
 * 
 * @author dell
 *
 */
public class TuofengCodeGens {

	public static String powerBy="Powered By 北京zscat科技, Since 2016 - 2020";
	public static String author="沈专";
	public static String email="951449465@qq.com";
	public static String version="1.0v";
	public static String date= new Date().toLocaleString();
	public static String description="商品";
	//模块名称
	public static String model="shop";
	//功能模块
	public static String entityClass="TGoods";
	//表明
	public static String tableName="t_goods";

		static String packagePree="com.stylefeng.guns.modular";


	   // 实体类变量名称
		public static String lowerentity=entityClass.substring(0, 1).toLowerCase()+entityClass.substring(1);
		public static String collName1;

	public static StringBuffer list1=new StringBuffer();
	public static StringBuffer list2=new StringBuffer();
	public static StringBuffer add=new StringBuffer();
	public static StringBuffer update=new StringBuffer();
	public static StringBuffer entitybuffer=new StringBuffer();
	public static StringBuffer collName=new StringBuffer();


	static String templatePath = "src/main/baseResources/template";

	public static void createControllerClass(String package1) throws IOException{
		String packages=package1.replace(".", "/");
		String newClassName=getHomeDir("src/main/java/"+packages)+model+"/controller"+"/"+entityClass+"Controller.java";
		String actionTempContent = readFile(getHomeDir(templatePath)+"controller.txt");
		new File(newClassName).getParentFile().mkdirs();
		if(!isExit(newClassName)){
			buildClass(actionTempContent, newClassName, packagePree);
			System.out.println(entityClass+":"+newClassName);
			String ss=actionTempContent;
		}else{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String line = reader.readLine();
			if(line != "" && line.equalsIgnoreCase("y")){
				buildClass(actionTempContent, newClassName, packagePree);
				System.out.println("覆盖新的实体实体类："+entityClass+"成功了");
			}
		}
	}

	public static void createEntityPath(String package1) throws IOException{
		String packages=package1.replace(".", "/");
		String newClassName = getHomeDir("src/main/webapp/WEB-INF/view")+model+"/"+lowerentity+"/"+lowerentity+".html";
		String actionTempContent = readFile(getHomeDir(templatePath)+"entityPage.txt");
		new File(newClassName).getParentFile().mkdirs();
		if(!isExit(newClassName)){
			buildClass(actionTempContent, newClassName, packagePree);
			System.out.println(entityClass+":"+newClassName);
			String ss=actionTempContent;
		}else{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String line = reader.readLine();
			if(line != "" && line.equalsIgnoreCase("y")){
				buildClass(actionTempContent, newClassName, packagePree);
				System.out.println("覆盖新的实体实体类："+entityClass+"成功了");
			}
		}
	}

	public static void createAddPath(String package1) throws IOException{
		String packages=package1.replace(".", "/");
		String newClassName = getHomeDir("src/main/webapp/WEB-INF/view")+model+"/"+lowerentity+"/"+lowerentity+"_add.html";
		String actionTempContent = readFile(getHomeDir(templatePath)+"addHtml.txt");
		new File(newClassName).getParentFile().mkdirs();
		if(!isExit(newClassName)){
			buildClass(actionTempContent, newClassName, packagePree);
			System.out.println(entityClass+":"+newClassName);
			String ss=actionTempContent;
		}else{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String line = reader.readLine();
			if(line != "" && line.equalsIgnoreCase("y")){
				buildClass(actionTempContent, newClassName, packagePree);
				System.out.println("覆盖新的实体实体类："+entityClass+"成功了");
			}
		}
	}

	public static void createEditPath(String package1) throws IOException{
		String packages=package1.replace(".", "/");
		String newClassName = getHomeDir("src/main/webapp/WEB-INF/view")+model+"/"+lowerentity+"/"+lowerentity+"_edit.html";
		String actionTempContent = readFile(getHomeDir(templatePath)+"editHtml.txt");
		new File(newClassName).getParentFile().mkdirs();
		if(!isExit(newClassName)){
			buildClass(actionTempContent, newClassName, packagePree);
			System.out.println(entityClass+":"+newClassName);
			String ss=actionTempContent;
		}else{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String line = reader.readLine();
			if(line != "" && line.equalsIgnoreCase("y")){
				buildClass(actionTempContent, newClassName, packagePree);
				System.out.println("覆盖新的实体实体类："+entityClass+"成功了");
			}
		}
	}

	public static void createEntityjs(String package1) throws IOException{
		String packages=package1.replace(".", "/");
		String newClassName = getHomeDir("src/main/webapp/static/modular")+model+"/"+lowerentity+"/"+lowerentity+".js";
		String actionTempContent = readFile(getHomeDir(templatePath)+"entityjs.txt");
		new File(newClassName).getParentFile().mkdirs();
		if(!isExit(newClassName)){
			buildClass(actionTempContent, newClassName, packagePree);
			System.out.println(entityClass+":"+newClassName);
			String ss=actionTempContent;
		}else{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String line = reader.readLine();
			if(line != "" && line.equalsIgnoreCase("y")){
				buildClass(actionTempContent, newClassName, packagePree);
				System.out.println("覆盖新的实体实体类："+entityClass+"成功了");
			}
		}
	}

	public static void createEntityINFO(String package1) throws IOException{
		String packages=package1.replace(".", "/");
		String newClassName = getHomeDir("src/main/webapp/static/modular")+model+"/"+lowerentity+"/"+lowerentity+"_info.js";
		String actionTempContent = readFile(getHomeDir(templatePath)+"entityinfo.txt");
		new File(newClassName).getParentFile().mkdirs();
		if(!isExit(newClassName)){
			buildClass(actionTempContent, newClassName, packagePree);
			System.out.println(entityClass+":"+newClassName);
			String ss=actionTempContent;
		}else{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String line = reader.readLine();
			if(line != "" && line.equalsIgnoreCase("y")){
				buildClass(actionTempContent, newClassName, packagePree);
				System.out.println("覆盖新的实体实体类："+entityClass+"成功了");
			}
		}
	}
	
	//将路径转换为/
	public static String conversionSpecialCharcters(String string){
		return string.replaceAll("\\\\", "/");
	}
	public static String getHomeDir(String path){
		if(!isEmpty(path)){
			return conversionSpecialCharcters(System.getProperty("user.dir"))+"/"+path+"/";
		}else{
			return System.getProperty("user.dir");
		}
	}
	
	public static void buildClass(String actionTempContent,String newFilepath,String pkg){
		actionTempContent= actionTempContent
				.replaceAll("\\[author\\]", author)
				.replaceAll("\\[date\\]", date)
				.replaceAll("\\[packagePree\\]",pkg )

				.replaceAll("\\[description\\]", description)
				.replaceAll("\\[model\\]",model )
				.replaceAll("\\[lowerentity\\]",lowerentity )
				.replaceAll("\\[capentity\\]", lowerentity)
				.replaceAll("\\[add\\]", add.toString())
				.replaceAll("\\[update\\]", update.toString())
				.replaceAll("\\[list1\\]", list1.toString())
				.replaceAll("\\[tablename\\]", tableName.toString())
				.replaceAll("\\[collName1\\]", collName1)
				.replaceAll("\\[list2\\]", list2.toString())
				.replaceAll("\\[entitybuffer\\]", entitybuffer.toString())
				
				.replaceAll("\\[powerBy\\]", powerBy)
				.replaceAll("\\[entityClass\\]", entityClass);
		
		writeFileByLine(actionTempContent,newFilepath);
	}
	private static void writeFileByLine(String actionTempContent,
			String filename) {
		File  file = new File(filename);
		PrintWriter write = null;
		
		try {
			write= new PrintWriter(new FileOutputStream(file));
			write.print(actionTempContent);
			write.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally{
			if(write != null){
				try{
				write.close();
				}catch(Exception e){}
			}
		}
		
		
	}
	public static boolean isExit(String filepath){
		File file= new File(filepath);
		return file.exists();
	}
	public static boolean isEmpty(String str){
		return str==null || str.length() == 0 || str.equals("")
				|| str.matches("\\s*");
	}
	
	public static String readFile(String filename){
		StringBuffer buffer =new StringBuffer();
		try{
			FileInputStream inputStream = new FileInputStream(new File(filename));
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			BufferedReader reader = new BufferedReader(inputStreamReader);
			String tempString = null;
			while((tempString = reader.readLine())!= null){
				buffer.append(tempString+"\n");
			}
			reader.close();
		}catch(Exception e){
			
		}
		return buffer.toString();
	}
	 public static String tuofeng(String str){
			if(str.indexOf("_")>-1){
				String c=str.substring(str.indexOf("_"));
				String ss= str.substring(0,str.indexOf("_"))+c.substring(1,2).toUpperCase()+c.substring(2);
				if(ss.indexOf("_")>-1){
					String cc=ss.substring(ss.indexOf("_"));
					return ss.substring(0,ss.indexOf("_"))+cc.substring(1,2).toUpperCase()+cc.substring(2);
				}else{
					return ss;
				}
			}else{
				return str;
			}
		}
	 public static  void getEntity( StringBuffer entity,String tableName, Connection connection) throws SQLException {
	        PreparedStatement pst = null;
	        ResultSet rs = null;
	        ResultSetMetaData rsd = null;
	       
	            //查询时没有数据，只返回表头信息
	            pst = connection.prepareStatement("select * from " + tableName + " where 1=2");
	            rsd = pst.executeQuery().getMetaData();

	            //查询主键
	            String primaryKey = null;
	            pst = connection
	                .prepareStatement("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_NAME='PRIMARY' and TABLE_NAME = ?");
	            pst.setString(1, tableName.toUpperCase());
	            rs = pst.executeQuery();
	            if (rs.next()) {
	                primaryKey = rs.getString(1);
	            }
	            
	            //查询列备注
	            pst = connection
	                .prepareStatement("select column_name, column_comment from information_schema.columns where table_name = ?");
	            pst.setString(1, tableName.toUpperCase());
	            rs = pst.executeQuery();

	            //先将注释放入到map再获取，防止有些列没有注释获取不对应的问题
	            Map<String, String> commentMap = new HashMap<String, String>();
	            while (rs.next()) {
	                commentMap.put(rs.getString("COLUMN_NAME"), rs.getString("column_comment"));
	            }

	            for (int i = 1; i <= rsd.getColumnCount(); i++) {
	                String name = tuofeng(rsd.getColumnName(i).toLowerCase());
	                String dbType = rsd.getColumnTypeName(i);
	                String javaT="";
	                if(!"id".equals(name.toLowerCase())){
	                	 if("VARCHAR".equals(dbType.toUpperCase()) ||"VARCHAR2".equals(dbType.toUpperCase())||"CHAR".equals(dbType.toUpperCase())){
			                	javaT="String";
			                }else if("DATETIME".equals(dbType.toUpperCase()) ||"DATE".equals(dbType.toUpperCase())||"timestamp".equals(dbType.toLowerCase())){
			                	javaT="Date";
			                }else if("int".equals(dbType.toUpperCase()) ||"INT".equals(dbType.toUpperCase()) ||"INTEGER".equals(dbType.toUpperCase())|| 
			                		"tinyint".equals(dbType.toLowerCase())){
			                	javaT="Integer";
			                }else if("decimal".equals(dbType.toUpperCase()) ||"DECIMAL".equals(dbType.toUpperCase())){
			                	javaT="BigDecimal";
			                }
			                else if("BIGINT".equals(dbType.toUpperCase()) ||"bigint".equals(dbType.toUpperCase())){
			                	javaT="Long";
			                }
			                else if("bit".equals(dbType.toUpperCase()) ||"BIT".equals(dbType.toUpperCase())){
			                	javaT="Boolean";
			                }else{
			                	javaT="Long";
			                }
			               
			             //   name=tuofeng(name);
			                String get=name.substring(0, 1).toUpperCase()+name.substring(1);
			              //  String javaT1=javaT.substring(0, 1).toUpperCase()+javaT.substring(1).toLowerCase();
			                
			                if("DATETIME".equals(dbType.toUpperCase()) ||"DATE".equals(dbType.toUpperCase())||"timestamp".equals(dbType.toLowerCase())){
			                	 entity.append("@DateTimeFormat( pattern = \"yyyy-MM-dd\" )\n");
			                }
			                entity.append(		"private "+javaT+" "+name+";\n");
			                entity.append(		"public "+ javaT+" get"+get+"() {return "+ "this.get"+javaT+"(\""+name+"\");}\n");
			                entity.append(		"public void set"+get+"("+javaT+" " +name+") {this.set(\""+name+"\","+name+");}\n");

	                }
	              
	              
	            }
	       
	        
	    }
	

	public static void main(String[] args) throws Exception {

		createControllerClass(packagePree);
		createAddPath(packagePree);
		createEditPath(packagePree);
		createEntityINFO(packagePree);
		createEntityjs(packagePree);
		createEntityPath(packagePree);
		
	}
	
	
	
}
